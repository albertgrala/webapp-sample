'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var rename = require('gulp-rename');
var livereload = require('gulp-livereload');
var gutil = require('gulp-util');

gulp.task('default',['sass','minify-css'], function(){
    // Default task
    return gutil.log('Everything packed!');
});

gulp.task('sass', function () {
    return gulp.src('./src/scss/styles.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./src/css'));
});

gulp.task('minify-css',['sass'], function() {
    return gulp.src('./src/css/styles.css')
        .pipe(cleanCSS())
        .pipe(rename({suffix :'.min'}))
        .pipe(gulp.dest('./src/css'))
        .pipe(gulp.dest('./public/css'))
        .pipe(livereload());
});


gulp.task('watch', function(){
    livereload.listen();
    gulp.watch('./src/scss/**/*.scss', ['sass', 'minify-css']);
    //gulp.watch('./public/*.html', livereload());
});